package demo

import groovy.json.JsonSlurper

class ExternJsonDataLoader implements DataLoader {
    URL externUrl

    List<Map> loadData(String region = "1084") {
        URL fullUrl = "${externUrl}?regionsnr=$region".toURL()
        new JsonSlurper().parse(fullUrl) as List
    }
}
