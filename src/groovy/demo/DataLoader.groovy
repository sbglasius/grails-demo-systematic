package demo

interface DataLoader {

    List<Map> loadData()
    List<Map> loadData(String region)
}