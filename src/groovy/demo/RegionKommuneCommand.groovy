package demo

import grails.validation.Validateable
import groovy.transform.ToString

@Validateable
@ToString(includeFields = true)
class RegionKommuneCommand {
    Kommune kommune
    Region region

    static constraints = {
        kommune(nullable: false)
        region(nullable: true)
    }
}
