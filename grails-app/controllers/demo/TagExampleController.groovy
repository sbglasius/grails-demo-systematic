package demo

class TagExampleController {

    def index() {

        [kommune: Kommune.get(1)]
    }

    def send() {
        sendMail {
            to 'kryf@plyf.dk'
            from 'soeren@glasius.dk'
            subject 'Grails course'
            body 'Body text'
        }
        render('ok  ')
    }
}
