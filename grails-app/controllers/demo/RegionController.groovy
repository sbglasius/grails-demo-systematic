package demo


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RegionController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Region.list(params), [status: OK]
    }

    @Transactional
    def save(Region regionInstance) {
        if (regionInstance == null) {
            render status: NOT_FOUND
            return
        }

        regionInstance.validate()
        if (regionInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        regionInstance.save flush: true
        respond regionInstance, [status: CREATED]
    }

    @Transactional
    def update(Region regionInstance) {
        if (regionInstance == null) {
            render status: NOT_FOUND
            return
        }

        regionInstance.validate()
        if (regionInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        regionInstance.save flush: true
        respond regionInstance, [status: OK]
    }

    @Transactional
    def delete(Region regionInstance) {

        if (regionInstance == null) {
            render status: NOT_FOUND
            return
        }

        regionInstance.delete flush: true
        render status: NO_CONTENT
    }
}
