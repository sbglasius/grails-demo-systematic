package demo

class RegionKommuneController {

    static defaultAction = "regionKommune"
    def regionKommune(RegionKommuneCommand regionKommuneCommand) {
        if (regionKommuneCommand.hasErrors()) {
            render("Has errors")

        } else {

            render(regionKommuneCommand.toString())
        }
    }
}
