package demo

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
@Secured(['ROLE_USER'])
class KommuneController {
    def kommuneService
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond kommuneService.getKommuner(params), model:[kommuneInstanceCount: kommuneService.count]
    }
//    Synthesized method of show(Kommune kommuneInstance)
//    def show() {
//        Long id = params.long('id')
//        return show(Kommune.get(id))
//    }


    @Secured(['ROLE_ADMIN'])
    def loadRegion(String region) {
        kommuneService.loadRegion(region)
        render('ok')
    }

    def show(Kommune kommuneInstance) {
        respond kommuneInstance
    }

    def create() {
        def kommune = new Kommune(params)
        respond kommune
    }

    @Transactional
    def save(Kommune kommuneInstance) {
        if (kommuneInstance == null) {
            notFound()
            return
        }

        if (kommuneInstance.hasErrors()) {
            respond kommuneInstance.errors, view:'create'
            return
        }

        kommuneInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'kommune.label', default: 'Kommune'), kommuneInstance.id])
                redirect kommuneInstance
            }
            rss {}
            '*' { respond kommuneInstance, [status: CREATED] }
        }
    }

    def edit(Kommune kommuneInstance) {
        respond kommuneInstance
    }

    @Transactional
    def update(Kommune kommuneInstance) {
        if (kommuneInstance == null) {
            notFound()
            return
        }

        if (kommuneInstance.hasErrors()) {
            respond kommuneInstance.errors, view:'edit'
            return
        }

        kommuneInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Kommune.label', default: 'Kommune'), kommuneInstance.id])
                redirect kommuneInstance
            }
            '*'{ respond kommuneInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Kommune kommuneInstance) {

        if (kommuneInstance == null) {
            notFound()
            return
        }

        kommuneInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Kommune.label', default: 'Kommune'), kommuneInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'kommune.label', default: 'Kommune'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
