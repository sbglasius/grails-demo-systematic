package demo

class QueryExampleController {

    def query() {
        Kommune.findAllByNavnIlike('A%')

        Kommune.withCriteria {
            ilike('navn','A%')
        }

        Kommune.where {
            navn ==~ ~/A.+/
            areal > 1000
        }.list()

    }
}
