class UrlMappings {

	static mappings = {
//        "rk/$kommune/$region?"(controller: 'regionKommune', action: 'regionKommune')

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }


        "/"(view:"/index")
        "500"(view:'/error')
	}
}
