import demo.ExternJsonDataLoader

// Place your Spring DSL code here
beans = {

    externDataSource(ExternJsonDataLoader) {
        externUrl = application.config.externData.url.toURL()
    }
}
