import demo.Kommune
import demo.Region
import demo.security.*
class BootStrap {
    def kommuneService

    def init = { servletContext ->
            bootstrapKommuner()

        def user = new Role("ROLE_USER").save(failOnError: true, flush: true)
        def admin = new Role("ROLE_ADMIN").save(failOnError: true, flush: true)

        def adminUser = new User("admin","password").save(failOnError: true, flush: true)

        UserRole.create(adminUser, admin)

    }
    def destroy = {
    }

    void bootstrapKommuner() {
        if(Kommune.count()) {
            return
        }
        kommuneService.loadData()
        new Region(nr: '0001', navn: 'Hovedstaden').save(failOnError: true)
    }
}
