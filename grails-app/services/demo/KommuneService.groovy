package demo
import grails.transaction.Transactional

@Transactional
class KommuneService {

    def externDataSource

    void loadData() {
        List json = externDataSource.loadData()
        storeData(json)
    }

    List<Kommune> getKommuner(Map params) {
        Kommune.list(params)
    }

    Long getCount() {
        Kommune.count()
    }

    void loadRegion(String region) {
        List json = externDataSource.loadData(region)
        storeData(json)
    }

    private Iterable<Map> storeData(List<Map> json) {
        json.each {
            new Kommune(navn: it.navn, nr: it.nr, areal: it.areal.toInteger() / 1000000).save(failOnError: true)
        }
    }

    Kommune getByNr(String nr) {
        Kommune.findByNr(nr)
    }
}
