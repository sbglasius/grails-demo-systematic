package demo

class Kommune {
    String nr
    String navn
    Integer areal
    String kontaktEmail
    String kontaktTelefon

    Region getRegion() {
        // Region.findByNr(regionsNr)
    }

    static transients = ['region']

    static constraints = {
        nr(nullable: false, unique: true)
        navn(nullable: false)
        areal(nullable: true)
        kontaktEmail(nullable: true, email: true)
        kontaktTelefon(nullable: true)
    }

    static mapping = {
        kontaktEmail column: 'email'
    }

    String toString() {
        "$nr: $navn ($areal)"
    }
}
