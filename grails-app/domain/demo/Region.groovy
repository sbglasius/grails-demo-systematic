package demo

import grails.rest.Resource

@Resource(uri =  '/region', formats = ['json'])
class Region {
    String nr
    String navn
    static constraints = {
    }
}
