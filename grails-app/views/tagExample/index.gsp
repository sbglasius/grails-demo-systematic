<%@ page import="demo.Kommune" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>

<body>

<h1>Tag Examples</h1>

<div>
    <k:simple input="${"<em>xxx</em>"}"/>
</div>

<hr/>
<div>
    <k:conditional condition="${true}">
        <p>I'm rendered</p>
    </k:conditional>
</div>
<hr/>

<div>
    ${demo.Kommune.findByNr('0108')}  <!-- Don't do this at home -->
</div>
<hr/>
<div>
    <k:kommune kommunenr="0101"/>
</div>
<hr/>
<k:menu>
    <k:item controller="kommune" action="index">Kommuner</k:item>
    <k:item controller="kommune" action="create">Opret kommune</k:item>
</k:menu>

</body>
</html>