<ul>
    <g:each in="${menuItems}" var="menuItem">
        <li>
            <g:if test="${menuItem.url}">
                <a href="${menuItem.url}">${menuItem.text}</a>
            </g:if>
            <g:else>
                <g:link controller="${menuItem.controller}" action="${menuItem.action}">${menuItem.text}</g:link>
            </g:else>
        </li>
    </g:each>
</ul>