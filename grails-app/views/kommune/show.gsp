<%@ page import="demo.Kommune" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'kommune.label', default: 'Kommune')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-kommune" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-kommune" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list kommune">

        <g:if test="${kommuneInstance?.nr}">
            <li class="fieldcontain">
                <span id="nr-label" class="property-label"><g:message code="kommune.nr.label" default="Nr"/></span>

                <span class="property-value" aria-labelledby="nr-label"><g:fieldValue bean="${kommuneInstance}" field="nr"/></span>

            </li>
        </g:if>

        <g:if test="${kommuneInstance?.navn}">
            <li class="fieldcontain">
                <span id="navn-label" class="property-label"><g:message code="kommune.navn.label" default="Navn"/></span>

                <span class="property-value" aria-labelledby="navn-label"><g:fieldValue bean="${kommuneInstance}" field="navn"/></span>

            </li>
        </g:if>

        <g:if test="${kommuneInstance?.areal}">
            <li class="fieldcontain">
                <span id="areal-label" class="property-label"><g:message code="kommune.areal.label" default="Areal"/></span>

                <span class="property-value" aria-labelledby="areal-label"><g:fieldValue bean="${kommuneInstance}" field="areal"/></span>

            </li>
        </g:if>

        <g:if test="${kommuneInstance?.kontaktEmail}">
            <li class="fieldcontain">
                <span id="kontaktEmail-label" class="property-label"><g:message code="kommune.kontaktEmail.label" default="Kontakt Email"/></span>

                <span class="property-value" aria-labelledby="kontaktEmail-label"><g:fieldValue bean="${kommuneInstance}" field="kontaktEmail"/></span>

            </li>
        </g:if>

        <g:if test="${kommuneInstance?.kontaktTelefon}">
            <li class="fieldcontain">
                <span id="kontaktTelefon-label" class="property-label"><g:message code="kommune.kontaktTelefon.label" default="Kontakt Telefon"/></span>

                <span class="property-value" aria-labelledby="kontaktTelefon-label"><g:fieldValue bean="${kommuneInstance}" field="kontaktTelefon"/></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: kommuneInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${kommuneInstance}"><g:message code="default.button.edit.label" default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
