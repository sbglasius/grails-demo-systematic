package demo

class KommuneTagLib {
    static defaultEncodeAs = [taglib:'html']
    static namespace = "k"
    static encodeAsForTags = [simple: [taglib:'html'], conditional: [taglib:'raw'], kommune: [taglib: 'raw'], menu: [taglib: 'raw']]

    def kommuneService
    def grailsApplication

    /**
     * <k:simple attr1="kryf"/>
     */
    def simple = { attrs ->
        def input = attrs.input
        log.error(input?.getClass()?.name)
        out << raw("<strong>")
        out << "Hej med dig: ${input}"
        out << raw("</strong>")
    }

    def conditional = { attrs, body ->
        def condition = attrs.boolean('condition')

        if(condition) {
            out << body()
        }
    }

    def kommune = { attrs, body ->
        def kommunenr = attrs.kommunenr
        def kommune = kommuneService.getByNr(kommunenr)

        out << g.render(template: '/tagTemplates/kommune', model: [kommune: kommune])

    }

    def menu = { attrs, body ->
        List menuItems = grailsApplication.config.defaultMenu ?: []
        pageScope.menuItems = menuItems
        body()

        out << g.render(template: '/tagTemplates/menu', model: [menuItems:menuItems])
    }

    def item = { attrs, body ->
        def controller = attrs.controller
        def action = attrs.action
        def menuItem = [controller: controller, action: action, text: body()]

        pageScope.menuItems << menuItem
    }

}
