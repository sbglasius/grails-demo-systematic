package demo



import grails.test.mixin.*
import spock.lang.*

@TestFor(KommuneController)
@Mock([Kommune, Region])
class KommuneControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        params["nr"] = '0001'
        params["navn"] = 'København'

    }

    def setup() {
        controller.kommuneService = Mock(KommuneService)
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            1 * controller.kommuneService.getKommuner(_) >> [new Kommune(), new Kommune()]
            1 * controller.kommuneService.getCount() >> 10
            model.kommuneInstanceList.size() == 2
            model.kommuneInstanceCount == 10
    }

    @Ignore
    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.kommuneInstance!= null
    }

    void "Test the save action correctly persists an instance with validation error"() {

        when: "The save action is executed with an invalid instance"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def kommune = new Kommune()
        kommune.validate()
        controller.save(kommune)

        then: "The create view is rendered again with the correct model"
        model.kommuneInstance != null
        view == 'create'

    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with a valid instance"
            populateValidParams(params)
            def kommune = new Kommune(params)

            controller.save(kommune)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/kommune/show/1'
            controller.flash.message != null
            Kommune.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def kommune = new Kommune(params)
            controller.show(kommune)

        then:"A model is populated containing the domain instance"
            model.kommuneInstance == kommune
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def kommune = new Kommune(params)
            controller.edit(kommune)

        then:"A model is populated containing the domain instance"
            model.kommuneInstance == kommune
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/kommune/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def kommune = new Kommune()
            kommune.validate()
            controller.update(kommune)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.kommuneInstance == kommune

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            kommune = new Kommune(params).save(flush: true)
            controller.update(kommune)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/kommune/show/$kommune.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/kommune/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def kommune = new Kommune(params).save(flush: true)

        then:"It exists"
            Kommune.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(kommune)

        then:"The instance is deleted"
            Kommune.count() == 0
            response.redirectedUrl == '/kommune/index'
            flash.message != null
    }
}
