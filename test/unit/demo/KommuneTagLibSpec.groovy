package demo

import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(KommuneTagLib)
class KommuneTagLibSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    void "test something"() {
        when:
        def output = applyTemplate('<k:simple input="${example}"/>', [example: input])

        then:
        output == expectedOutput

        where:
        input  | expectedOutput
        'kryf' | '<strong>Hej med dig: kryf</strong>'
        null   | '<strong>Hej med dig!</strong>'
    }
}
