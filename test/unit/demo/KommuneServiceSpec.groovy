package demo

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification
import spock.lang.Unroll

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(KommuneService)
@Mock(Kommune)
class KommuneServiceSpec extends Specification {

    def setup() {
        service.externDataSource = Mock(DataLoader)
    }

    @Unroll
    void "test default data loader for #description kommuner"() {
        when:
        service.loadData()

        then:
        1 * service.externDataSource.loadData() >> json
        Kommune.count() == expectedCount

        where:
        json                                                                      || expectedCount | description
        []                                                                        || 0             | "Zero"
        [[nr: '1', navn: 'a', areal: '123']]                                      || 1             | "One"
        [[nr: '1', navn: 'a', areal: '123'], [nr: '12', navn: 'b', areal: '234']] || 2             | "Two"
    }

    void "test default data loader when it throws an exception"() {
        when:
        service.loadData()

        then:
        1 * service.externDataSource.loadData() >> { throw new IOException("Could not load data") }
        thrown(Exception)
    }
}
