package demo

import grails.test.spock.IntegrationSpec
import spock.lang.Unroll

class KommuneServiceIntegrationSpec extends IntegrationSpec {
    def kommuneService



    @Unroll
    def "test default loading of data"() {
        given:
        def savedDataLoader = kommuneService.externDataSource
        kommuneService.externDataSource = Mock(DataLoader)
        Kommune.list()*.delete(flush: true) // Delete needs to happen BEFORE the test is execute, as Bootstrap has already loaded the data!

        when:
        kommuneService.loadData()

        then:
        1 * kommuneService.externDataSource.loadData() >> json
        Kommune.count() == expectedCount

        cleanup:
        kommuneService.externDataSource = savedDataLoader

        where:
        json                                                                      | expectedCount | description
        []                                                                        | 0             | "Zero"
        [[nr: '1', navn: 'a', areal: '123']]                                      | 1             | "One"
        [[nr: '1', navn: 'a', areal: '123'], [nr: '12', navn: 'b', areal: '234']] | 2             | "Two"

    }

    def "test default loading of data from external source"() {
        setup:
        Kommune.list()*.delete(flush: true) // Delete needs to happen BEFORE the test is execute, as Bootstrap has already loaded the data!

        when:
        kommuneService.loadData()

        then:
        Kommune.count() == 29
    }

}
